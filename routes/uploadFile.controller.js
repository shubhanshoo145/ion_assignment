const express = require('express');
const router = express.Router();
const fs = require('fs');
const Readable = require('stream').Readable;

function uploadFileController(req, res) {
  let stream = fs.createReadStream(`${process.env.ROOT_DIRECTORY}${req.file.path}`);
  let previousElem = ''
  let jsonData = [];
  stream.on('data', (data) => {
    let batch = data.toString()
    let lastIndexOfOpenBrace = batch.lastIndexOf('{')
    let batchData = batch.substring(0, lastIndexOfOpenBrace - 1)
    const isOpenSquareBracket = batchData.split('[').length > 1
    const isCloseSquareBracket = batchData.split(']').length > 1
    batchData = batchData.trim()
    batchData = batchData.substring(batchData.length-1) === ',' ? batchData.substring(0,batchData.length-1): batchData
    if (previousElem) {
      batchData = previousElem.concat(batchData)
    }
    previousElem = batch.substring(lastIndexOfOpenBrace) // last incomplete object
    if (isOpenSquareBracket) {
      batchData = `${batchData}]`
    } else if (isCloseSquareBracket) {
      batchData = `[${batchData}`
    } else {
      batchData = `[${batchData}]`
    }
    jsonData = jsonData.concat(JSON.parse(batchData));
    const rstream = new Readable()
    rstream._read = function noop() { }
    rstream.push(JSON.stringify(jsonData))
    rstream.push(null)
    rstream.pipe(res, { end: false })
  });
  stream.on('end', () => {
    res.send()
  })
}

router.post('/uploadFile', uploadFileController)

module.exports = router;